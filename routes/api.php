<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', [
    'namespace' => 'App\Http\Controllers\Api'
], function ($api) {
    //登录注册节流限制
    $api->group([
        'middleware' => 'api.throttle', 'limit' => 100, 'expires' => 5
    ], function ($api) {
        $api->post('captchas',"UsersController@captureShow")->name('api.users.captureShow');
        $api->post('verifyCodes',"UsersController@verifyCodes")->name('api.users.verifyCodes');
    });

    $api->group([
        //访问节流策略
        'middleware' => 'api.throttle', 'limit' => 100, 'expries' => 5
    ], function ($api) {

        //需要认证的接口
        $api->group(['middleware' => 'api.auth'], function ($api) {

        });
    });
});
