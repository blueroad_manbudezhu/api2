<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Overtrue\EasySms\EasySms;

class TestController extends Controller
{
    //
    public function index()
    {
        $config=config('easysms');
        $easySms=new EasySms($config);
        $easySms->send(15548551950,[
           // 'content'=>'您的验证码是${code},有效期为5分钟,请勿泄露给他人',
           'template'=>'SMS_109340195',
            'data'=>[
                'code'=>12334
            ]
        ]);
    }
}
